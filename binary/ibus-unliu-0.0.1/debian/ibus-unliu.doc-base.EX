Document: ibus-unliu
Title: Debian ibus-unliu Manual
Author: <insert document author here>
Abstract: This manual describes what ibus-unliu is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/ibus-unliu/ibus-unliu.sgml.gz

Format: postscript
Files: /usr/share/doc/ibus-unliu/ibus-unliu.ps.gz

Format: text
Files: /usr/share/doc/ibus-unliu/ibus-unliu.text.gz

Format: HTML
Index: /usr/share/doc/ibus-unliu/html/index.html
Files: /usr/share/doc/ibus-unliu/html/*.html
