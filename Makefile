ALL			=	YahooKeyKey fcitx
CMD_IBUS	=	ibus-table-createdb

all: $(ALL)

ibus:
	$(CMD_IBUS) -n unliu.db -d -s unliu.cin

fcitx: ime/fcitx/unliu.txt

YahooKeyKey: ime/YahooKeyKey/unliu.cin

ime/fcitx/unliu.txt: unliu.core.txt
	cat ime/fcitx/head > $@
	cat unliu.core.txt >> $@

ime/YahooKeyKey/unliu.cin: unliu.core.txt
	cat ime/YahooKeyKey/head > $@
	cat unliu.core.txt >> $@
