<?
// 輸入法表格製作工具 for UNLiu (1.00)
// 作者: $Author: anton $ (anton.tw@gmail.com)
// 版本: $Rev: 18 $ $Date: 2008-01-01 14:36:42 +0800 (二, 01  1月 2008) $
require("inc/dict.php");
require("inc/class.db.php");
require("inc/db_config.php");

$fn="ibus/header";
$fs=fopen($fn, "r");
$content = fread($fs, filesize($fn));
fclose($fs);
echo $content;
$db = new DBData();
$db->INIT();
$sql = "SELECT `codeno`, `keymap`, `keyorder` FROM `UNLiu`".
	"ORDER BY `keymap`,`keyorder` DESC";
$result = $db->SQL($sql);
$rownum = count($result);

echo "BEGIN_TABLE\n";
for ($i = 0; $i < $rownum; $i++) {
	$value = intval($result[$i]["keyorder"]);
	if ($value < 0)
		$value = 0;
	$word = uni2utf8($result[$i]["codeno"]);
	if ($word != "0") 
		echo $result[$i]["keymap"] . "\t" . $word . "\t" . $value . "\n";
}
echo "END_TABLE\n";
$db->END();
?>
