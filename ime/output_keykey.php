<?
// vim:set fdm=indent ts=2 ai et sts=2 sw=2 tw=0 filetype=php:
// 輸入法表格製作工具 for UNLiu (1.00)
// 作者: $Author: anton $ (anton.tw@gmail.com)
// 版本: $Rev: 18 $ $Date: 2008-01-01 14:36:42 +0800 (二, 01  1月 2008) $
require("inc/dict.php");
require("inc/class.db.php");
$fn="keykey.head";
$fs=fopen($fn,"r");
$content=fread($fs,filesize($fn));
fclose($fs);
echo $content;
$db = new DBData();
include("inc/db_config.php");
$db->INIT();
$sql="SELECT distinct(`keymap`) FROM `UNLiu_a` ORDER BY `keymap` DESC";
$result = $db->SQL($sql);
$rownum = count($result);

echo "%chardef begin\n";
for ($i = 0; $i < $rownum; $i++)
{
	$sql="SELECT `codeno` FROM `UNLiu_a` WHERE `keymap`='".
		$result[$i][keymap]."' ORDER BY `keyorder` DESC";
	$keylist = $db->SQL($sql);
	$keynum = count($keylist);
	for ($x = 0; $x < $keynum; $x++)
	{
		$word = uni2utf8($keylist[$x][codeno]);
		if ($word!="0")
			echo $result[$i][keymap]." $word\n";
	}
}
echo "%chardef end\n";
$db->END();
?>
