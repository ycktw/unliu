<?
function weather_content($city)
{
    $prefix = "http://www.cwb.gov.tw/rss/forecast/36_";
    $ans = "";
    $no = 0;

    $list = "台北市 高雄市 基隆北海岸 台北 桃園 新竹 苗栗 台中 彰化 南投 " .
		"雲林 嘉義 台南 高雄 屏東 恆春 宜蘭 花蓮 台東 澎湖 金門 馬祖";
    $list = explode(" ", $list);
    for ($i = 0; $i < count($list); $i ++) {
        if ($list[$i] == $city) {
            break; 
        }   
    } 
    $no = $i + 1;
    $url = sprintf("%s%02d.xml", $prefix, $no);
    $content = my_curl($url);

	return $content;
}

function get_tag($node, $tagName)
{
	for ($i = 0 ; $i < sizeof($node->tagChildren); $i ++) {
		if ($node->tagChildren[$i]->tagName == $tagName)
			return $node->tagChildren[$i];
	}
	return null;
}

function get_tags($node, $tagName)
{
	$ret = array();

	for ($i = 0 ; $i < sizeof($node->tagChildren); $i ++) {
		if ($node->tagChildren[$i]->tagName == $tagName) {
			array_push($ret, $node->tagChildren[$i]);
		}
	}
	return $ret;
}

function parse_body($xml)
{
	$c = get_tag($xml, "channel");
	$item = get_tag($c, "item");
	$ans = get_tag($item, "description");

	return $ans->tagData;
}

function weather($city)
{
	$xml = weather_content($city);
	$parser = new XMLParser($xml);
	$parser -> Parse();
	$content = parse_body($parser->document);
	return $content;
}

?>
