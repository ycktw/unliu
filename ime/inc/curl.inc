<?
function my_curl($url, $referer = NULL)
{
    $ch = curl_init($url);
    curl_setopt ($ch, CURLOPT_ENCODING, 0);
    curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_HEADER, 1);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 30);
	if ($referer != NULL)
		curl_setopt ($ch, CURLOPT_REFERER, $referer); 
    $content = curl_exec($ch);

    if (curl_errno($ch)) 
        return null;

    curl_close($ch);
	if ($content) {
		$content = strstr($content, "\r\n\r\n");
		$content = substr($content, 4);
	}
    return $content;
}
?>
