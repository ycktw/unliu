<?
class Cell {
  var $pid = -1 ;
  var $status_now  = STATUS_UNKNOWN;
  var $status_last = STATUS_UNKNOWN;
  var $time2sleep  = 60;
  var $time2exit   = 120;
  var $time2call   = TIME2CALL;
  var $mesg_in     = "";
  var $mesg_out    = "";

  function GetMesg()
  {
    $ans = $mesg_in;
    $mesg_in = "";
    return $ans;
  }

  function PutMesg($mesg)
  {
    $mesg_out = $mesg;
  }
}
?>
