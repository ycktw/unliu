<?

// simple database query class by $Author: anton $
// $Date: 2008-02-26T03:31:53.279063Z $
// class.db.php
// 資料庫程式 功能定義 $URL
//  Released as a GPL licensed software.

class DBData {

	var $dbname = "";
	var $hostname = "localhost";
	var $username = "admin";
	var $password = "password";
	var $linkid = NULL;
	var $encoding = NULL;
	var $dbt = NULL;
	var $rownum = 0;

	function INIT() {
		global $dbset;

		// load settings
		if (isset($dbset)) {
			$this->dbname = $dbset["dbname"];
			$this->hostname = $dbset["hostname"];
			$this->username = $dbset["username"];
			$this->password = $dbset["password"];
			$this->encoding = $dbset["encoding"];
		}

		$id = @mysql_connect($this->hostname, $this->username, $this->password);
		if ($id) {
			$this->linkid = $id;
			$status = mysql_select_db($this->dbname);
		} else {
			return ERROR_CONNECTION_FAILED;
		}

		if (!$status) {
			return ERROR_DB_SELECTION_FAILED;
		} else {
			return FALSE;
		}
	}

	function END() {
		if ($this->linkid)
			$status = @mysql_close($this->linkid);
		if (!$status) {
			return ERROR_DB_CLOSED_FAILED;
		} else {
			return FALSE;
		}
	}

	function SQL($sql) {
		$data = NULL;

		if ($this->encoding)
			@mysql_query("SET NAMES '" . $this->encoding . "'", $this->linkid);
		else
			@mysql_query("SET NAMES 'UTF8'", $this->linkid);

		$this->dbt = @mysql_query($sql, $this->linkid);
		if (!$this->dbt)
			return NULL;

		if ($this->rownum = @mysql_num_rows($this->dbt)) {
			$data = Array();
			while ($rows = mysql_fetch_assoc($this->dbt)) {
				$data[] = $rows;
			}
		}
		return $data;
	}

}

?>
