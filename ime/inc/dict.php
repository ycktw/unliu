<?
/* vim:set fdm=indent ts=4 ai et sts=4 sw=4 tw=0:
 *  simple database query class by $Author: anton $
 *  $Date: 2008-02-26 11:31:53 +0800 (二, 26  2月 2008) $
 *  字典程式 功能定義 $URL
 *  Released as a GPL licensed software.
 */

define (STOCK_INTERVAL,300);

function stock_data($no)
{
    $needtoget = false;
    $now = time();
    $now = ($now+TZSTAMP);
    if (!$no)
        $no="2382";

    $fst_existed=file_exists("temp/$no");
    if ($fst_existed) {
        $fst = stat("temp/$no");
        $fct = $fst[ctime];
        if (($now - $fct) < STOCK_INTERVAL )
        {
            $needtoget = false;
        } else if ( ($now%86400) > 48600) /* 48600==13:30,32400==9:00 午後 */
        {
            /* 如果同一天 就看 $fct 有沒有超過 13:30 有就不抓 */
            if (
	  	        ( ceil($now / 86400) == ceil($fct/86400)) &&
	  	        ( $fct%86400) > 48600
		        ) {
                $needtoget = false;
            } else {
                $needtoget = true;
            }
        } else if ( ($now%86400) < 32400 ) {
            if (ceil($now/86400)==ceil($fct/86400))
            {
                $needtoget = false;
            } else if (ceil($now/86400) - ceil($fct/86400) >= 1) {
                $needtoget = true;
            } else if (($fct%86400) < 48600) {
                $needtoget = true;
            } else {
                $needtoget = false;
            }
        } else {
            $needtoget = true;
        }
    } else {
        $needtoget = true;
    }

    if ($needtoget) {
        $url="http://mis.tse.com.tw/data/$no.csv?ID=$now"."00";
        $fs=fopen($url,"r");
        $content="";
        if ($fs)
        {
            while(!feof($fs)) {
                $content.=fread($fs,1024);
            }
            @fclose($fs);
        }
        $content = iconv("Big5","UTF-8",$content);
        if (strstr("錯誤",$content))
            return "錯誤的代號";
        $fs=@fopen("temp/$no","w");
        @fputs($fs, $content);
        @fclose($fs);
    } else {
        $url="temp/$no";
        $fs = @fopen($url,"r");
        $content="";
        while(!feof($fs))
            $content .= fread($fs,1024);
        @fclose($fs);
    }
    $content = stripslashes($content);
    $cols = explode(",", $content);
    $chopstring = Array(" ","\"");
    for ($i = 0; $i < count($cols); $i++ )
    {
        $cols[$i] = str_replace($chopstring,"",$cols[$i]);
    }

    $cols[32] = rtrim($cols[32]);
    return $cols;
}

function stock_price($no)
{
    $data = stock_data($no);
    return $data[8]; /* 成交價 */
}

function stock($no)
{
    $data = stock_data($no);
    $data[32] = rtrim($data[32]); /* name chop */
    if ($data[8]==$data[3])
        $mark="⊕";
    else if ($data[8]==$data[4])
        $mark="◒";
    else if ($data[1]>0)
        $mark="▲";
    else if ($data[1]<0)
        $mark="▼";
    else
        $mark="=";

    $ans = "$data[32] [$data[0]]".
        "更新時間: $data[2]\n".
        "成交價: $data[8] 元\n".
        "當盤成交量: $data[10] 張\n".
        "當日成交量: $data[9] 張\n".
        "今日漲跌: $mark  $data[1] 元\n".
        "今日最高價: $data[6] 元\n".
        "今日最低價: $data[7] 元\n".
        "委買量: $data[12] 張\n".
        "委賣量: $data[22] 張\n";
    $ans = str_replace("?", "0", $ans);
    return $ans;
}

function jpdict($word)
{
    $word = iconv("UTF-8","EUCJP",$word);
    $url = "http://dictionary.goo.ne.jp/search.php?MT=".
            urlencode($word)."&kind=all&mode=0&kwassist=0";
    $content = file_get_contents($url);
    iconv_set_encoding("internal_encoding", "UTF-8");
    iconv_set_encoding("output_encoding", "UTF-8");
    $content = iconv("EUCJP","UTF-8",$content);
    $content = str_replace("\r","",$content);
    $content = str_replace("\n","",$content);
    $tag = "diclst1";
    preg_match_all("/<!--".$tag."-->(.*?)<!--\/".$tag."-->/",
            $content, $matches);
    $matches=$matches[1];
    $ans = "";
    for ($i=0;$i<count($matches);$i++) {
        $ans .= str_replace("&nbsp;"," ",strip_tags($matches[$i]))."\n";
    }
    return (($ans) ? $ans : "查無結果】");
}

function dict($word)
{
    if (strstr($word," ")) return;
    if (strstr($word,";")) return;

    exec("/usr/bin/sdcv -n $word",$out);
    $ans="";
    $sum = count($out);
    for ($i=0;$i<$sum;$i++) {
    if (!strstr($out[$i],"save to cache"))
        $ans.=$out[$i]."\n";
    }
    exec("tool/tdic.sh $word",$out);
    $sum = count($out);
    for ($i=0;$i<$sum;$i++) {
        $ans.=$out[$i]."\n";
    }
    $ans = htmlspecialchars($ans);
    return $ans;
}

function UNLiu($word)
{
    global $CangJieline,$Array30line,$ZhuYinline;
    global $db;

    $z_orig = Array(
        ",", "-", ".", "/", "0", "1", "2", "3", "4", "5",
        "6", "7", "8", "9", ";", "a", "b", "c", "d", "e",
        "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
        "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
        "z"
    );
    $z_subst = Array(
        "ㄝ", "ㄦ", "ㄡ", "ㄥ", "ㄢ", "ㄅ", "ㄉ", "ˇ", "ˋ", "ㄓ",
        "ˊ", "˙", "ㄚ", "ㄞ", "ㄤ", "ㄇ", "ㄖ", "ㄏ", "ㄎ", "ㄍ",
        "ㄑ", "ㄕ", "ㄘ", "ㄛ", "ㄨ", "ㄜ", "ㄠ", "ㄩ", "ㄙ", "ㄟ", "ㄣ",
        "ㄆ", "ㄐ", "ㄋ", "ㄔ", "ㄧ", "ㄒ", "ㄊ", "ㄌ", "ㄗ", "ㄈ"
    );
    $c_orig = Array(
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
        "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
        "u", "v", "w", "x", "y", "z"
    );
    $c_subst= Array(
        "日", "月", "金", "木", "水", "火", "土", "竹", "戈", "十",
        "大", "中", "一", "弓", "人", "心", "手", "口", "尸", "廿",
        "山", "女", "田", "難", "卜", "重"
    );
    $a_orig = Array(
        ",", ",", ".", ".", "/", "/", "0", "1", "2", "3",
        "4", "5", "6", "7", "8", "9", ";", ";", "a", "b",
        "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
        "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z"
    );
    $a_subst = Array(
        "8v", "8v", "9v", "9v", "0v", "0v", "0", "1", "2",
        "3", "4", "5", "6", "7", "8", "9", "0-", "0-", "1-",
        "5v", "3v", "3-", "3^", "4-", "5-", "6-", "8^", "7-",
        "8-", "9-", "7v", "6v", "9^", "0^", "1^", "4^", "2-", "5^",
        "7^", "4v", "2^", "2v", "6^", "1v"
    );
    mb_internal_encoding("UTF-8");
    $len  = mb_strlen($word);
    if ($len == 0)
        return "請輸入 #no [查詢的字串]";
    $ans ="";

    if (is_numeric($word))
    {
        $ans.="unicode 十進位碼轉換後為: '".uni2utf8($word)."'\n";
        $ans.="unicode 十六進位碼轉換後為: '".uni2utf8(hexdec($word))."' \n";
    } else {
        for ($x = 0 ; $x < $len ; $x++)
        {
            $ans .= unl(mb_substr($word, $x, 1));
        }
        $sum  = count($ZhuYinline);
        for ($x=0;$x<$len;$x++) {
            $w = mb_substr($word,$x,1);
            $found = false;
            $ans.="[$w] 的注音是：\n";
            for ($i=0;$i<$sum-1;$i++) {
                $col = explode(" ",$ZhuYinline[$i]);
                if ($col[1]==$w) {
                    $ans.=str_replace($z_orig,$z_subst,$col[0])." ";
                    $found = true;
                }
            }
            if ($found)
                $ans.="\n";
            else
                $ans.="找不到合適的注音。\n";
        }

      $sum  = count($CangJieline);
      for ($x=0;$x<$len;$x++)
      {
          $w = mb_substr($word,$x,1);
          $found = false;
          $ans.="[$w] 的倉頡碼是：\n";
          for ($i=0;$i<$sum-1;$i++)
          {
              $col = explode(" ",$CangJieline[$i]);
              if ($col[1]==$w)
              {
                  $ans.=str_replace($c_orig,$c_subst,$col[0])." ";
                  $found = true;
              }
          }
          if ($found)
              $ans.="\n";
          else
              $ans.="找不到合適的倉頡碼。\n";
      }

      $sum  = count($Array30line);
      for ($x=0;$x<$len;$x++)
      {
          $w = mb_substr($word,$x,1);
          $found = false;
          $ans.="[$w] 的行列碼是：\n";
          for ($i=0;$i<$sum-1;$i++)
          {
              $col = explode(" ",$Array30line[$i]);
              if ($col[1]==$w)
              {
                  $ans.=str_replace($a_orig,$a_subst,$col[0])." ";
                  $found = true;
              }
          }
          if ($found)
              $ans.="\n";
          else
              $ans.="找不到合適的行列碼。\n";
      }
    }
    return $ans;
}

function unl_find($key = "key", $idx)
{
    global $db;
    $len = 0;

    $ans = "";
    switch ($key){
    default:
    case "key":
        $sql = "SELECT `codeid`, `codeno`, `keyorder` FROM `UNLiu_a` ".
            "WHERE `keymap`='". addslashes($idx) . "'".
            "ORDER BY `keyorder` DESC";
        break;
    }
    $data = $db->SQL($sql);
    $len = count($data);
    if ($len > 0) {
        $ans .= "找尋 key ($idx) 值 ($len 個結果): \n";
        for ($i = 0; $i < $len; $i ++) {
            $order = $data[$i][keyorder] + 1;
            $ans .= uni2utf8($data[$i][codeno]). "(".
                $data[$i][keyorder]. "/". $data[$i][codeno].
                "/".$data[$i][codeid]." ) \n";
        }
    }
    return $ans;
}

function unl_shift($keymap, $order)
{
    function _update_order($codeid, $order)
    {
        global $db;
        if ($order < 0)
            $order = 0;
        $sql = "UPDATE `UNLiu_a` SET `keyorder` = '$order' ".
            " WHERE `codeid`='". $codeid ."'";
        return $db->SQL($sql);
    }

    function _get_key($keymap)
    {
        global $db;
        $sql = "SELECT `codeid`, `codeno`, `keyorder`, `keymap` ".
            "FROM `UNLiu_a` WHERE `keymap`='".
            addslashes($keymap)."' ORDER BY `keyorder` DESC";
        return $db->SQL($sql);
    }
    global $db;

    mb_internal_encoding("UTF-8");
    if ($order == "")
        $order = 5;
    if ($contributor == "")
        $contributor = "NULL";
    $ans ="";
    $data = _get_key($keymap);
    $len = count($data);
    if ($len == 0) {
        $ans .= "查無此鍵\n";
    } else {
        for ($i = 0; $i < $len; $i++) {
            $ans .= "更新 ". uni2utf8($data[$i][codeno]). " ".
                $data[$i][keymap]. " 順序：".
                ($data[$i][keyorder] - $order). "\n";
            _update_order($data[$i][codeid], ($data[$i][keyorder] - $order));
        }
    }
    return $ans;
}

function unl_add($word, $keymap, $order, $contributor)
{
    function _add($word, $keymap, $order, $contributor)
    {
        global $db;

        $is_num = is_numeric($word);
        $ans = "新增對應中\n";

        if ($is_num) {
            $sql = "INSERT INTO `UNLiu_a` ".
                "(`codeno`, `keymap`, `keyorder`, ".
                "`createtime`, `contributor`) ".
                "VALUES ('$word', '". addslashes($keymap)."', ".
                "'$order', NOW(), '$contributor');";
        } else {
            $sql = "INSERT INTO `UNLiu_a` ".
                "(`codeno`, `keymap`, `keyorder`, ".
                "`createtime`, `contributor`) ".
                "VALUES ('".utf2uni($word)."', '". addslashes($keymap) ."', ".
                "'$order', NOW(), '$contributor');";
        }
        $db->SQL($sql);
        $ans .= "由 $contributor 新增 ($word, $keymap, $order) 完畢\n";
        return $ans;
    }

    function _get_dup($word, $keymap, $order)
    {
        global $db;

        $is_num = is_numeric($word);
        if (!$is_num) {
            $sql = "SELECT `keymap`, `keyorder` ".
                "FROM `UNLiu_a` WHERE `codeno`='".
                utf2uni($word). "' AND `keymap`='".
                addslashes($keymap)."' AND `keyorder`='$order' ";
        } else {
            $sql = "SELECT `keymap`, `keyorder` ".
                "FROM `UNLiu_a` WHERE `codeno`='$word' AND `keymap`='".
                addslashes($keymap)."' AND `keyorder`='$order' ";
        }
        return $db->SQL($sql);
    }

    function _get_key($keymap)
    {
        global $db;

        $sql = "SELECT `codeno`, `keyorder` ".
            "FROM `UNLiu_a` WHERE `keymap`='".
            addslashes($keymap)."' ORDER BY `keyorder` DESC";
        return $db->SQL($sql);
    }
    global $db;

    mb_internal_encoding("UTF-8");
    if ($order == "")
        $order = 30;
    if ($contributor == "")
        $contributor = "NULL";
    $is_num = is_numeric($word);
    $len  = mb_strlen($word);
    if ($len != 1 && !$is_num)
        return "請輸入 #unladd [字($word/".
        utf2uni($word).")] [碼($keymap)] [序($order)]";
    $ans ="";
    $data = _get_dup($word, $keymap, $order);
    $len = count($data);
    if ($len == 0) {
        /* 檢查 keymap 有沒有重複 */
        $data = _get_key($keymap); $len = count($data);
        if ($len == 0) {
            $ans .= _add($word, $keymap, $order, $contributor);
        } else {
            /* 如果已有 keymap ，檢查有無 word 重複 */
            $no = ($is_num)? $word: utf2uni($word);
            for ($i = 0; $i < $len; $i++) {
                if ($data[$i][codeno] == $no) {
                    $ans .= "已有重複項目，取消新增\n";
                    $ans .= "DEBUG: ". $data[0][keymap]. ",".
                        $data[0][keyorder]." \n";
                    return $ans;
                }
                if ($order == $data[$i][keyorder]) {
                    $ans .= "有重複順序，自動減1\n";
                    $order = $data[$i][keyorder] - 1;
                } else {
                    $ans .= "比對 $order '".$data[$i][keyorder]."' \n";
                }
            }
            $ans .= _add($word, $keymap, $order, $contributor);
        }
    } else {
        $ans .= "已有重複項目，取消新增\n";
        $ans .= "DEBUG: ". $data[0][keymap]. ",".
            $data[0][keyorder]." \n";
    }
    return $ans;
}

function unl_del($word, $keymap)
{
    global $db;

    mb_internal_encoding("UTF-8");
    $len  = mb_strlen($word);
    $is_num = is_numeric($word);
    if ($len != 1 && !$is_num)
        return "請輸入 #unldel [字($word)] [碼($keymap)]";
    $ans ="";
    if (!$is_num) {
        $sql = "SELECT `keymap`, `keyorder` FROM `UNLiu_a` WHERE `codeno`='".
            utf2uni($word). "' AND `keymap`='$keymap' ";
    } else {
        $sql = "SELECT `keymap`, `keyorder` FROM `UNLiu_a` WHERE `codeid`='".
            $word. "' AND `keymap`='$keymap' ";
    }
    $data = $db->SQL($sql);
    if (count($data) != 0)
    {
        $ans .= "刪除對應中\n";
        if (!$is_num)
        {
            $sql = "DELETE FROM `UNLiu_a` WHERE ".
                "`codeno`='". utf2uni($word). "' AND `keymap`='$keymap'";
        } else {
            $sql = "DELETE FROM `UNLiu_a` WHERE ".
                "`codeid`='$word' AND `keymap`='$keymap'";
        }
        $db->SQL($sql);
        $ans .= "刪除完畢\n";
        $ans .= "DEBUG: ". $data[0][keymap]. ",". $data[0][keyorder]." \n";
    } else {
        $ans .= "查無該條目，取消刪除\n";
        $ans .= "請輸入 #unldel [漢字] [鍵盤對應碼] 或 \n";
        $ans .= "請輸入 #unldel [codeid] [鍵盤對應碼]\n";
    }
    return $ans;
}

function unl_ls($codeno)
{
    function codeno_get_key($codeno)
    {
        global $db;

        $sql = "SELECT `codeno`, `keyorder`, `keymap` ".
            "FROM `UNLiu_a` WHERE `codeno`='".
            $codeno. "' ORDER BY `keyorder` DESC";
        return $db->SQL($sql);
    }
    global $db;

    if ( !is_numeric($codeno) )
        return "不合法的數值";
    $ans = "\n";
    for ($i = 0; $i < 5; $i ++)
    {
        $data = codeno_get_key($codeno + $i);
        $ans .= uni2utf8($codeno + $i) . " (". ($codeno + $i). ") ";
        for ($x = 0; $x < count($data); $x++)
        {
            $ans .= $data[$x][keymap]. " ";
        }
        $ans .= "\n";
    }
    return $ans;
}

function unl($word)
{
    global $db;

    mb_internal_encoding("UTF-8");
    $len  = mb_strlen($word);
    if ($len == 0)
        return "USAGE: command [WORD]";
    $ans ="";

    if ( is_numeric($word) )
    {
        $ans .= "unicode 十進位碼轉換後為: '".uni2utf8($word)."'\n";
        $ans .= "unicode 十六進位碼轉換後為: '".uni2utf8(hexdec($word))."' \n";
    } else {
        for ($x = 0 ; $x < $len ; $x++)
        {
            $w = mb_substr($word, $x, 1);
            $found = false;
            $sql = "SELECT `keymap`, `contributor`, `keyorder` FROM `UNLiu_a` WHERE `codeno`='". utf2uni($w)."' ORDER BY `keyorder` DESC;\n";
            $ans .= "[$w] unicode 是： ".utf2uni($w).
                " hex： ".dechex(utf2uni($w))."\n";
            $ans .= "[$w] 的開放蝦米碼是：\n";
            $data = $db->SQL($sql);
            $found = (count($data) > 0 );
            for ($i = 0 ; $i < count($data); $i++)
            {
                $ans .= $data[$i][keymap]." (".
                    $data[$i][contributor]. ") ".$data[$i][keyorder]."\n";
            }
            if ($found)
                $ans .= "\n";
            else
                $ans .= "找不到合適的開放蝦米碼。\n";
        }
    }
    return $ans;
}

function utf2uni($utf8_char)
{
    $ch=ord(substr($utf8_char,0,1));
    if ($ch<0x80) return $ch;
    if ($ch>0xBF && $ch<0xFE)
    {
        if ($ch<0xE0)
        {
            $i=1;
            $uni_code=$ch-0xC0;
        }
        elseif ($ch<0xF0)
        {
            $i=2;
            $uni_code=$ch-0xE0;
        }
        elseif ($ch<0xF8)
        {
            $i=3;
            $uni_code=$ch-0xF0;
        }
        elseif ($ch<0xFC)
        {
            $i=4;
            $uni_code=$ch-0xF8;
        }
        else
        {
            $i=5;
            $uni_code=$ch-0xFC;
        }
    }
    else
    {
        echo "UTF-8 code error!!";
        exit;
    }

    for ($j=0;$j<$i;$j++)
    {
        $ch = ord(substr($utf8_char,$j+1,1))-0x80;
        $uni_code = $uni_code*64 + $ch;
    }
    return $uni_code;
}

function uni2utf8($c)
{
    $str="";
    if ($c < 0x80)
    {
        $str.=$c;
    }
    else if ($c < 0x800)
    {
        $str.=chr(0xC0 | $c>>6);
        $str.=chr(0x80 | $c & 0x3F);
    }
    else if ($c < 0x10000)
    {
        $str.=chr(0xE0 | $c>>12);
        $str.=chr(0x80 | $c>>6 & 0x3F);
        $str.=chr(0x80 | $c & 0x3F);
    }
    else if ($c < 0x200000)
    {
        $str.=chr(0xF0 | $c>>18);
        $str.=chr(0x80 | $c>>12 & 0x3F);
        $str.=chr(0x80 | $c>>6 & 0x3F);
        $str.=chr(0x80 | $c & 0x3F);
    }
    return $str;
}

function cols($time = NULL, $period = 1)
{
    function time_get_data($time, $period = 1)
    {
        global $db;

        $sql = "SELECT `id`, `jid`, UNIX_TIMESTAMP(`time`), ".
            "`money`, `description` ".
            "FROM `money` WHERE `time`>". ($time - ($period * 86400)).
            " ORDER BY `time` DESC";
        return $db->SQL($sql);
    }
    global $db;

    date_default_timezone_set('Asia/Taipei'); // buggy
    if (!is_numeric($time) && ($time != NULL))
        return "不合法的數值";
    $ans = "\n";
    $data = time_get_data($time, $period);
    for ($i = 0; $i < count($data); $i++) {
        $flag = (substr($data[$i][money], 0, 1) == '-') ? "收入" : "支出";
        if ($flag == "收入")
            $money = str_replace('-', '', $data[$i][money]);
        else
            $money = $data[$i][money];
        $ans .= $data[$i][id].":".strftime("%Y/%m/%d", $data[$i][time]);
        $ans .= "$flag ".number_format($money)." ".
            $data[$i][description]."\n";
    }
    return $ans;
}

function coadd($money, $description, $time = NULL, $owner)
{
    function _add($money, $year = NULL, $mon = NULL, $day = NULL,
        $description, $owner)
    {
        global $db;

        $ans = "";
        $is_num = is_numeric($money);
        if (!$is_num)
            return "不合法的金錢數值";

        $flag = (substr($money, 0, 1) == '+') ? "收入" : "支出";
        if ($flag == "收入")
            $money = str_replace('+', '-', $money);
        if ($mon == NULL || $day == NULL) {
            $time = strftime("%Y-%m-%d %H:%M:%S",
                time());
        } else {
            $time = strftime("%Y-%m-%d %H:%M:%S",
                mktime(0, 0, 0, $mon, $day, $year));
        }
        $sql = "INSERT INTO `money` ".
            "(`jid`, `time`, `money`, `description`)".
            " VALUES('$owner', '$time', '$money', '$description')";
        $db->SQL($sql);
        if ($mon)
            $ans .= "$flag $money 的記錄($year/$mon/$day)($description)\n";
        else
            $ans .= "$flag $money 的記錄($description)\n";
        return $ans;
    }
    global $db;

    mb_internal_encoding("UTF-8");
    if (!is_numeric($money)) {
        $is_plus = (substr($money, 0, 1) == '+');
        if (!$is_plus)
            return "不合法的金錢數值";
    }
    $cols = split('/', $time);
    if (count($cols) == 3)
        return _add($money, $cols[0], $cols[1], $cols[2],
            $description, $owner);
    else if (count($cols) == 2)
        return _add($money, strftime("%y", time()), $cols[0], $cols[1],
            $description, $owner);
    else
        return _add($money, NULL, NULL, NULL,
            $description, $owner);
}

function codel($id, $owner)
{
    function _del($id, $owner)
    {
        global $db;

        $ans = "";
        $is_num = is_numeric($id);
        if (!$is_num)
            return "不合法的ID";

        $sql = "DELETE FROM `money` ".
            " WHERE `jid`='$owner' AND `id`='$id'";
        $db->SQL($sql);
        return "刪除 $id 的記錄";
    }
    global $db;

    mb_internal_encoding("UTF-8");
    if (!is_numeric($id)) {
        return "不合法的 ID";
    }
    return _del($id, $owner);
}
?>
