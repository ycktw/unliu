<?
// vim:set fdm=indent ts=2 ai et sts=2 sw=2 tw=0 filetype=php:
// 輸入法表格製作工具 for UNLiu (1.00)
// 作者: $Author: anton $ (anton.tw@gmail.com)
// 版本: $Rev: 18 $ $Date: 2008-01-01 14:36:42 +0800 (二, 01  1月 2008) $
require("inc/dict.php");
require("inc/class.db.php");

define (DIRTY_TABLE, 0); /* if is dirty_table */
define (MAXNO, 30); 

$fn = $argv[1];
$content = file_get_contents($fn);

/* Handling Table Header and footer */
$tempbuf = strstr($content, "BEGIN_TABLE");
if ($tempbuf)
{
	echo "Normal CIN Table found, start to parsing\n";
	$content = substr($tempbuf, 12);
	$content_length = strlen($content);
	if (substr($content, -10) == "END_TABLE\n")
		$content = substr($content, 0, $content_length - 10);
}
/* Handling Table Header and footer End */

$lines = explode("\n", $content);
echo "Sum: ".count($lines)." lines\n";
$db = new DBData();
include("inc/db_config.php");
$db->INIT();
for ($i = 0; $i < count($lines); $i++)
{
	if (($i + 1) % 50 == 0)
		echo ": $i \n";
	$cols = explode("\t", $lines[$i]);
	$keymap = $cols[0];
	$codeno = utf2uni($cols[1]);
	$keyorder = $cols[2];

	/* find if this keymap already inputed */
	$sql = "SELECT `codeno`, `keymap`, `keyorder` FROM `UNLiu_a` ".
		"WHERE `keymap` = \"$keymap\" ".
		"ORDER BY `keyorder` DESC, `codeno` DESC";
	$result = $db->SQL($sql);
	$rownum = count($result);
	if ($rownum == 0 || $result == null)
	{
		echo "A";
		/* if not inputed, insert by keyorder */
		$sql = "INSERT INTO `UNLiu_a` (`codeno`, `keymap`, `keyorder`) ".
		"VALUES('$codeno', \"$keymap\", '".MAXNO."')";
		$result = $db->SQL($sql);
		continue;
	} else {
		/* keymap exists */
		$is_found = false;
		$is_30_found = false;
		$is_29_found = false;
		$maxno_u = 0;
		for ($x = 0; $x < $rownum; $x++)
		{
			if ($result[$x][codeno] == $codeno) /* this rule is inputed */
			{
				$is_found = true;
				break;
			}
			if ($result[$x][keyorder] == 30)
				$is_30_found = true;
			if ($result[$x][keyorder] == 29)
				$is_29_found = true;
			$maxno_u = $result[$x][keyorder] - 1;
		}
		if ($is_found)
		{
			echo "."; /* this rule is inputed */
		} else {
		/*
			if ($is_30_found == false)
				$maxno = 30;
			else if ($is_29_found == false)
				$maxno = 29;
			else */
				$maxno = $maxno_u;
			$sql = "INSERT INTO `UNLiu_a` (`codeno`, `keymap`, `keyorder`) ".
				"VALUES('$codeno', \"$keymap\", '$maxno')";
			$db->SQL($sql);
			echo "A";
		}
	}
}
$db->END();
?>
